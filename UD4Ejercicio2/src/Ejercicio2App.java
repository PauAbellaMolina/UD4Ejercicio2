public class Ejercicio2App {
	public static void main(String[] args) {
		final int N = 10;
		final double A = 5.2;
		final char C = 'z';
		
		//Valor de cada variable
		System.out.println("Variable N = " + N);
		System.out.println("Variable A = " + A);
		System.out.println("Variable C = " + C);
		
		//Suma de N+A
		System.out.println(N + " + " + A + " = " + N+A);
		
		//Diferencia de A-N
		System.out.println(A + " - " + N + " = " + (A-N));
		
		//Valor num�rico correspondiente al car�cter que contiene la variable C
		System.out.println("Valor num�rico del car�cter " + C + " = " + (int)C);
	}
}